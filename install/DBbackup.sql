
-- Dumping structure for table foreo.foreo_product_master
DROP TABLE IF EXISTS `foreo_product_master`;
CREATE TABLE IF NOT EXISTS `foreo_product_master` (
  `ProductId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(250) NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`ProductId`),
  FULLTEXT KEY `ProductName` (`ProductName`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

-- Dumping data for table foreo.foreo_product_master: 5 rows
/*!40000 ALTER TABLE `foreo_product_master` DISABLE KEYS */;
INSERT INTO `foreo_product_master` (`ProductId`, `ProductName`, `Created`) VALUES
	(123, 'Foreo Ilva', '2016-02-18'),
	(10, 'FOREO Luna', '2016-03-04'),
	(13, 'FOREO Issa', '2016-03-04'),
	(15, 'FOREO Iris', '2016-03-04'),
	(33, 'gfdcb dgfed', '2016-03-04');
/*!40000 ALTER TABLE `foreo_product_master` ENABLE KEYS */;


-- Dumping structure for table foreo.foreo_stock_master
DROP TABLE IF EXISTS `foreo_stock_master`;
CREATE TABLE IF NOT EXISTS `foreo_stock_master` (
  `StockId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantiy` int(11) NOT NULL,
  `Type` varchar(20) NOT NULL COMMENT 'CC order || OS order || Other',
  `Created` date NOT NULL,
  `LastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`StockId`),
  KEY `ProductId` (`ProductId`),
  KEY `Type` (`Type`),
  KEY `Quantiy` (`Quantiy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table foreo.foreo_stock_master: 4 rows
/*!40000 ALTER TABLE `foreo_stock_master` DISABLE KEYS */;
INSERT INTO `foreo_stock_master` (`StockId`, `ProductId`, `Quantiy`, `Type`, `Created`, `LastUpdated`) VALUES
	(110, 13, 200, 'OS Order', '2016-03-04', '2016-03-23 16:45:28'),
	(100, 10, 100, 'CC Order', '2016-03-04', '2016-03-23 16:44:04'),
	(120, 15, 150, 'Other', '2016-03-04', '2016-03-23 16:45:28'),
	(23, 33, 32, 'Other', '2016-06-04', '2016-03-23 16:53:41');
	
-- Dumping structure for procedure foreo.foreo_save_stock
DROP PROCEDURE IF EXISTS `foreo_save_stock`;
DELIMITER //
CREATE PROCEDURE `foreo_save_stock`(IN `Stock_ID` INT, IN `Product_ID` INT, IN `Product_Name` VARCHAR(250), IN `Quantites` INT, IN `OrderType` VARCHAR(20), IN `CreatedDate` DATE)
    COMMENT 'To add/edit Stock master'
BEGIN
	DECLARE Error INT(1) DEFAULT 0;
	DECLARE isStock INT(11) DEFAULT NULL;
	DECLARE isProduct INT(11) DEFAULT NULL;
	# CHECK for existing data
	SELECT COUNT(*) INTO isStock FROM foreo_stock_master fsm where fsm.StockId = Stock_ID;
	SELECT COUNT(*) INTO isProduct FROM foreo_product_master fpm where fpm.ProductId = Product_ID;
	START TRANSACTION;	
		# Add Data for Product if not exist
		IF isProduct IS NULL OR isProduct < 1 THEN
			INSERT INTO foreo_product_master  (
				ProductId,
				ProductName,
				Created
			) VALUES (
				Product_ID,
				Product_Name,
				CreatedDate
			);
		END IF;
		# Add Data for Stock if not exist
		IF isStock IS NULL OR isStock < 1 THEN
			INSERT INTO foreo_stock_master  (
				StockId,
				ProductId,
				Quantiy,
				`Type`,
				Created
			) VALUES (
				Stock_ID,
				Product_ID,
				Quantites,
				OrderType,
				CreatedDate
			);
		
		ELSE
		# update Stock if exist record for StockID
			UPDATE foreo_stock_master  SET
				ProductId = Product_ID,
				Quantiy = Quantites,
				Created = CreatedDate, `Type` = OrderType  WHERE StockId = Stock_ID;
				
		END IF;
		SELECT ROW_COUNT() INTO Error; # get count of effected rows
	COMMIT;
	
	#SELECT Error; # To return message about success/failure
END//
DELIMITER ;
