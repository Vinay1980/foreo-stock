README
===============================================================================================================
Author: VINAY KANT SAHU
Email: vinaykant.sahu@gmail.com
Created Date: Mar 23, 2016
===============================================================================================================
This is very basic Core PHP script written from scratch, to run this system please follow below steps
1. Thank your for download. :)
2. Keep this folder under the webroot folder 
wamp: c://wamp/www/foreo
lamp: /var/html/www/foreo
3. Create a Database using MySQL
4. Now run http://localhost/foreo/install/installation.php for initial setup.
5. Please follow the instructions on installation page. 
6. URL To run script : http://localhost/foreo

There is also a sample xlsx file (sample/Stock.xlsx) available for ref.

--------------------- Asignment Task -----------------
Hi Vinay Kant,

According to our last agreement I'm sending you one task so I can see your work.

I will not go into to much details with requirements and you are free to decide on your own about any question you could have. I would suggest to do this using plain PHP and not use any high-end framework (Zend/Symofony/...). However, you can use any PHP library you need if you want to. 

Please deliver you final work in a form of GitHub/Bitbucket repository; just send me URL of that repository.

User story
====================
Me as a user of this web application
would like to have a possibility to import data in a form of Excel document
so I can manage my data more quickly and efficiently.


Excel columns / Mysql table
====================
Stock ID - integer
Product ID - integer
Product name - varchar
Quantity - integer
Type - varchar (CC order || OS order || Other)
Created - ISO date
(there is an example file in attachment)


Business logic
====================
When doing import, if there is existing record (based on Stock ID column) than we want to update record. If not, than we are inserting a new record.


User interface
====================
Just one screen with three sections:
1 - Upload form - to upload Excel document
2 -  Filter/Search form to filter data based on "Type" and "Created" values
3 - Table to display all database records. Table should have pager with 50 items per page. Order criteria is column "Created".

Kind regards

------------------------------------------------
Please feel free to contact if any issue using cell +918959692591 or email vinaykant.sahu@gmail.com

