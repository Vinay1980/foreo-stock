<?php
if(is_writable ('../Connections/Connection.php') ){
	header("Location: error.php?msg=Connection/Connection.php file is writable, please make it read only."); die;
} 
if(is_dir('install')){
	header("Location: error.php?msg=Please remove 'install' folder to avoid any hack."); die;
}