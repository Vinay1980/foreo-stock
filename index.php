<?php require_once('Connections/Connection.php');
require_once('check.php'); ?>
<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_stocklist = 50;
$pageNum_stocklist = 0;
if (isset($_GET['pageNum_stocklist'])) {
  $pageNum_stocklist = $_GET['pageNum_stocklist'];
}
$startRow_stocklist = $pageNum_stocklist * $maxRows_stocklist;
$RecordCount =$pageNum_stocklist * $maxRows_stocklist + 1 ;
if(isset($_GET['order']) && $_GET['order']){
	$order =$_GET['order'];
}else{
	$order = 'desc';
}
 
$query_stocklist = "SELECT FSM.StockId, FSM.ProductId, FSM.Quantiy, FSM.Type, FSM.Created, FPM.ProductName   FROM foreo_stock_master FSM INNER JOIN foreo_product_master FPM ON FPM.ProductId = FSM.ProductId WHERE 1 ";
if(isset($_GET['Created']) && $_GET['Created']){
	$query_stocklist .= sprintf(" AND FSM.Created =%s",GetSQLValueString($_GET['Created'],'date'));
}
if(isset($_GET['Type']) && $_GET['Type']){
	$query_stocklist .= sprintf(" AND FSM.Type =%s",GetSQLValueString($_GET['Type'],'text'));
}
 $query_stocklist .= " ORDER BY FSM.Created $order";
 
$query_limit_stocklist = sprintf("%s LIMIT %d, %d", $query_stocklist, $startRow_stocklist, $maxRows_stocklist);
$stocklist = mysql_query($query_limit_stocklist, $Connection) or die(mysql_error());
//$row_stocklist = mysql_fetch_assoc($stocklist);

if (isset($_GET['totalRows_stocklist'])) {
  $totalRows_stocklist = $_GET['totalRows_stocklist'];
} else {
  $all_stocklist = mysql_query($query_stocklist);
  $totalRows_stocklist = mysql_num_rows($all_stocklist);
}
$totalPages_stocklist = ceil($totalRows_stocklist/$maxRows_stocklist)-1;

$queryString_stocklist = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_stocklist") == false && 
        stristr($param, "totalRows_stocklist") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_stocklist = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_stocklist = sprintf("&totalRows_stocklist=%d%s", $totalRows_stocklist, $queryString_stocklist);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home Page</title>

<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>
 <link rel="stylesheet" href="css/style.css" />
  <script src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/calendar.js"></script>
</head>

<body><table width="90%" border="0" align="center" cellpadding="5" cellspacing="5" style="min-height: 400px;">
  <tr>
    <td rowspan="3">&nbsp;</td>
    <td align="center"><strong>Foreo Company</strong></td>
    <td rowspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><form action="importXlsx.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#8E8E8E">
        <tr>
          <td nowrap="nowrap"  align="center" style="color:#FFF;">Import Xlsx file to update Stock</td>
          </tr>
          <tr>
          <td nowrap="nowrap" align="center"><label for="StockFile"></label>
            <input type="file" name="StockFile" id="StockFile" />
            <input type="submit" name="button" id="button" value="Update Now" /></td>
           
        </tr>
      </table>
    </form>
   <h4> Search</h4>
   <form action="" method="get"  name="srcFrm" id="srcFrm">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr>
          <td nowrap="nowrap"  align="center" > 
            Created Date:<input type="text" name="Created" id="Created" class="date-picker"  value="<?php if(isset($_GET['Created']) && $_GET['Created']){echo $_GET['Created']; }?>" /> &nbsp;&nbsp;&nbsp;Type:<select name="Type">
			<option value="" <?php if(!isset($_GET['Type']) || $_GET['Type']==''){echo ' selected="selected"'; }?>>All</option>
			<option value="CC order" <?php if(!isset($_GET['Type']) || $_GET['Type']=='CC order'){echo ' selected="selected"'; }?>>CC order</option>
			<option value="OS order" <?php if(!isset($_GET['Type']) || $_GET['Type']=='OS order'){echo ' selected="selected"'; }?>>OS order</option>
			<option value="Other" <?php if(!isset($_GET['Type']) || $_GET['Type']=='Other'){echo ' selected="selected"'; }?>>Other</option>
			</select>
            <input type="submit" name="Submit1" id="Submit1" value="Search" />  </td>
           
        </tr>
      </table>
    </form>
	<h4> Stock List</h4>
    <table width="100%" border="0" cellspacing="10" cellpadding="5">
  <tr bgcolor="#CCCCCC" >
    <th nowrap="nowrap"><span class="style1">#</span></th>
    <th nowrap="nowrap"><span class="style1">Stock ID</span></th>
    <th nowrap="nowrap"><span class="style1">Product ID</span></th>
    <th nowrap="nowrap"><span class="style1">Product Name</span></th>
    <th nowrap="nowrap"><span class="style1">Quantity</span></th>
    <th nowrap="nowrap"><span class="style1">Type</span></th>
    <th nowrap="nowrap"><span class="style1">Created</span></th>
  </tr>
  <?php while ($row_stocklist = mysql_fetch_assoc($stocklist)) { ?>
    <tr>
      <td align="center"><?php echo $RecordCount++;?></td>
      <td align="center"><?php echo $row_stocklist['StockId']; ?></td>
      <td align="center"><?php echo $row_stocklist['ProductId']; ?></td>
      <td align="center"><?php echo $row_stocklist['ProductName']; ?></td>
      <td align="center"><?php echo $row_stocklist['Quantiy']; ?></td>
      <td align="center"><?php echo $row_stocklist['Type']; ?></td>
      <td align="center"><?php echo $row_stocklist['Created']; ?></td>
    </tr>
    <?php } ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4" align="center">&nbsp;
      <table width="255" border="0">
        <tr>
          <td width="60"><?php if ($pageNum_stocklist > 0) { // Show if not first page ?>
              <a href="<?php printf("%s?pageNum_stocklist=%d%s", $currentPage, 0, $queryString_stocklist); ?>">First</a>
              <?php } // Show if not first page ?></td>
          <td width="60"><?php if ($pageNum_stocklist > 0) { // Show if not first page ?>
              <a href="<?php printf("%s?pageNum_stocklist=%d%s", $currentPage, max(0, $pageNum_stocklist - 1), $queryString_stocklist); ?>">Previous</a>
              <?php } // Show if not first page ?></td>
          <td width="60"><?php if ($pageNum_stocklist < $totalPages_stocklist) { // Show if not last page ?>
              <a href="<?php printf("%s?pageNum_stocklist=%d%s", $currentPage, min($totalPages_stocklist, $pageNum_stocklist + 1), $queryString_stocklist); ?>">Next</a>
              <?php } // Show if not last page ?></td>
          <td width="58"><?php if ($pageNum_stocklist < $totalPages_stocklist) { // Show if not last page ?>
              <a href="<?php printf("%s?pageNum_stocklist=%d%s", $currentPage, $totalPages_stocklist, $queryString_stocklist); ?>">Last</a>
              <?php } // Show if not last page ?></td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
</tr>
</table>
</td>
  </tr>
  <tr>
    <td align="center">&copy; Foreo <?php echo date("Y");?> &nbsp;</td>
  </tr>
</table>

</body>
</html>
<?php
mysql_free_result($stocklist);
?>
